//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/jquery-migrate/dist/jquery-migrate.min.js
//= ../../node_modules/svg4everybody/dist/svg4everybody.min.js

// List of Bootstrap JS Components
//= ../../node_modules/bootstrap/js/transition.js
//= ../../node_modules/bootstrap/js/collapse.js
// //= ../../node_modules/bootstrap/js/modal.js
//= ../../node_modules/bootstrap/js/tab.js

// Responsive Carousel Slider =>
//= ../../node_modules/owl.carousel/dist/owl.carousel.min.js

// Rellax – Plagin For Parallax Effects
// //= ../../node_modules/rellax/rellax.min.js

// AOS – Animate on Scroll Library
// //= ../../node_modules/aos/dist/aos.js

// Fancybox
//= ../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js

$(document).ready(function() {

  svg4everybody();

  $(".categories__slider").owlCarousel({
    margin: 10,
    dots: false,
    nav: true,
    navText: ['<svg width="36" height="12" viewBox="0 0 36 12" xmlns="http://www.w3.org/2000/svg"><title>Arrow</title><g fill-rule="evenodd"><path d="M36 5H10v1h26z"/><path d="M10 0L0 5.735l10 5.734"/></g></svg>', '<svg width="36" height="12" viewBox="0 0 36 12" xmlns="http://www.w3.org/2000/svg"><title>Arrow</title><g fill-rule="evenodd"><path d="M36 5H10v1h26z"/><path d="M10 0L0 5.735l10 5.734"/></g></svg>'],
    stagePadding: 1,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      992: {
        items: 4,
        mouseDrag: false
      }
    }
  });

  $('.companies__slider').owlCarousel({
    margin: 10,
    dots: false,
    nav: true,
    navText: ['<svg width="36" height="12" viewBox="0 0 36 12" xmlns="http://www.w3.org/2000/svg"><title>Arrow</title><g fill-rule="evenodd"><path d="M36 5H10v1h26z"/><path d="M10 0L0 5.735l10 5.734"/></g></svg>', '<svg width="36" height="12" viewBox="0 0 36 12" xmlns="http://www.w3.org/2000/svg"><title>Arrow</title><g fill-rule="evenodd"><path d="M36 5H10v1h26z"/><path d="M10 0L0 5.735l10 5.734"/></g></svg>'],
    stagePadding: 1,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 3
      },
      992: {
        items: 6,
        mouseDrag: false
      }
    }
  });

  $('.similar-products__slider').owlCarousel({
    margin: 10,
    dots: false,
    nav: true,
    navText: ['<svg width="36" height="12" viewBox="0 0 36 12" xmlns="http://www.w3.org/2000/svg"><title>Arrow</title><g fill-rule="evenodd"><path d="M36 5H10v1h26z"/><path d="M10 0L0 5.735l10 5.734"/></g></svg>', '<svg width="36" height="12" viewBox="0 0 36 12" xmlns="http://www.w3.org/2000/svg"><title>Arrow</title><g fill-rule="evenodd"><path d="M36 5H10v1h26z"/><path d="M10 0L0 5.735l10 5.734"/></g></svg>'],
    stagePadding: 1,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 3
      },
      992: {
        items: 4,
        mouseDrag: false
      }
    }
  });

  $("[data-fancybox]").fancybox({
    // Options will go here
  });

});